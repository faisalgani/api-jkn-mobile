<?php 
include('./config.php');
// echo $url_aplikasi;
// die();
$url=$url_aplikasi.'index.php/';
      $url_assests=$url_aplikasi; ?> 
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title> <?php echo $nama_aplikasi; ?> </title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo $url_assests;?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo $url_assests;?>assets/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo $url; ?>">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-code"></i>
        </div>
       
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo $url.'dashboard/update'; ?>">
          <i class="fa fa-list" aria-hidden="true"></i>
          <span>Update</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      

      <!-- Nav Item - Pages Collapse Menu -->
    <!-- <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
         <i class="fas fa-coins"></i>
          <span>Service</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="<?php echo $url . 'rest_server/generade/'; ?>">Generade Token</a>
          </div>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="<?php echo $url . 'rest_server/generade/'; ?>">No Antrian</a>
          </div>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="<?php echo $url . 'rest_server/generade/'; ?>">Rekap Antrian</a>
          </div>
        </div>
         <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="<?php echo $url . 'rest_server/generade/'; ?>">List Booking Operasi</a>
          </div>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="<?php echo $url . 'rest_server/generade/'; ?>">List Jadwal Operasi</a>
          </div>
        </div>
      </li> -->
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo $url.'dashboard'; ?>">
          <i class="fa fa-cog"></i>
          <span>Service</span></a>
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      

      <!-- Nav Item - Pages Collapse Menu -->
      

      <!-- Nav Item - Charts -->
      
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
          <h3><?php echo $nama_aplikasi; ?></h3>
        </nav>

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
          <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
          
          </form>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>         

        </nav>
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Service</h1>
           
          </div>

          <!-- Content Row -->
          <div class="row">

            <!-- Content Column -->
            

<div id="accordion" class="col-lg-12" >
   <div class="card">
    <div class="card-header">
      <a class="card-link" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
      Get Token 
      </a>
    </div>
    <div id="collapseOne" class="collapse">
      <div class="card-body">
              <h3><i>{BASE URL}</i>/<i>Nama Service/</i>index.php/auth/login</h3>
              <br />
              <p>Fungsi : Generate Token</p>
              <p>Method : <b>POST</b></p>
              <p>Format : <b>Json</b></p>
              <p>Parameter : <b>Customer dan Key</b></p>
              <div id="getTokenParam">
              <pre>
                <span class="cp">                               
       {
          "customer" :"79106",
          "key" :"12345"
       }
              </span>
              </pre>
             </div> 
              <p>Response</b></p>
              <div id="getToken">
              <pre>
              <span class="cp">                               
      {
          "response": {
              "token": "7aAN1zyWNUFSg51"
          },
          "metadata": {
              "message": "Ok",
              "code": 200
          }
      }          
      </span>
    </pre>
      </div>
      <br>
      <button class="btn btn-danger" data-clipboard-target="#getTokenParam" data-clipboard-action="copy">
          Copy to clipboard
      </button>
    </div>
   </div> 
  </div>
  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo2">
       Get No Antrian
      </a>
    </div>
    <div id="collapseTwo2" class="collapse">
      <div class="card-body">
          <h3><i>{BASE URL}</i>/<i>Nama Service/</i>index.php/antrian</h3>
          <br />
          <p>Fungsi : Mengambil nomor antrean</p>
          <p>Method : <b>POST</b></p>
          <p>Format : <b>Json</b></p>
          <p>Security   : <b>Token</b></p>
          <p>Header:     <b>Content-Type : application/json </b></p>
          <p>Header:     <b>x-token : token </b></p>
          <p>Header:     <b>x-customer : customer </b></p>
          <p>Parameter  1 : <b>nomorkartu</b></p>
          <p>Parameter  2 : <b>nik</b></p>
          <p>Parameter  3 : <b>notelp</b></p>
          <p>Parameter  4 : <b>tanggalperiksa </b></p>
          <p>Parameter  5 : <b>kodepoli</b></p>
          <p>Parameter  6 : <b>nomorreferensi</b></p>
          <p>Parameter  7 : <b>jenisreferensi</b></p>
          <p>Parameter  8 : <b>jenisrequest</b></p>
          <p>Parameter  9 : <b>polieksekutif</b></p>
          <div id="getNoAntrianParam">
              <pre>
                <span class="cp">                               
      {
        "nomorkartu" : "0002614278846",
        "nik" : "0",
        "notelp": "0815468995801",
        "tanggalperiksa" : "2020-09-11",
        "kodepoli": "011",
        "nomorreferensi": "0001R0040116A000001",
        "jenisreferensi": "2",
        "jenisrequest": "2",
        "polieksekutif":"0"
     }
              </span>
              </pre>
             </div> 
               <p>Response</b></p>
           <div id="getNoAntrian">
            <pre>
              <span class="cp">
{
    "response": [
        {
            "no_antrian": 1,
            "no_boking": "B-204",
            "jenis_antrian": 2,
            "estimasidilayani": "61260000",
            "namapoli": "Penyakit Dalam",
            "namadokter": "Alfred Situmorang, dr, Sp.PD"
        }
    ],
    "metadata": {
        "message": "Ok",
        "code": 200
    }
}
              </span>
           </pre> 
           <br>
          <button class="btn btn-danger" data-clipboard-target="#getNoAntrianParam" data-clipboard-action="copy">
              Copy to clipboard
          </button>    
      </div>
    </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo3">
      Get Rekap Antrian
      </a>
    </div>
    <div id="collapseTwo3" class="collapse">
      <div class="card-body">
         <h3><i>{BASE URL}</i>/<i>Nama Service/</i>index.php/antrian/rekap_antrian</h3>
          <br />
          <p>Fungsi : Mengambil rekap antrean harian</p>
          <p>Method : <b>POST</b></p>
          <p>Format : <b>Json</b></p>
          <p>Security   : <b>Token</b></p>
          <p>Header:     <b>Content-Type : application/json </b></p>
          <p>Header:     <b>x-token : token </b></p>
          <p>Header:     <b>x-customer : customer </b></p>
          <p>Parameter  1 : <b>tanggalperiksa</b></p>
          <p>Parameter  2 : <b>kodepoli</b></p>
          <p>Parameter  3 : <b>polieksekutif</b></p>
          <div id="getRekapAntrianParam">
              <pre>
                <span class="cp">                               
     {
        "tanggalperiksa" : "2020-08-24",
        "kodepoli" : "001",
        "polieksekutif" : "0"
    }
              </span>
              </pre>
             </div> 
               <p>Response</b></p>
           <div id="getRekapAntrian">
            <pre>
              <span class="cp">
{
    "response": [
        {
            "namapoli": "Jantung",
            "totalantrean": 45,
            "jumlahterlayani": 45,
            "lastupdate": "2780633000"
        }
    ],
    "metadata": {
        "message": "Ok",
        "code": 200
    }
}
              </span>
           </pre> 
           <br>
          <button class="btn btn-danger" data-clipboard-target="#getRekapAntrianParam" data-clipboard-action="copy">
              Copy to clipboard
          </button>    
      </div>
    </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo4">
      Get List Kode Booking Operasi
      </a>
    </div>
    <div id="collapseTwo4" class="collapse">
      <div class="card-body">
         <h3><i>{BASE URL}</i>/<i>Nama Service</i>/index.php/operasi/kode_booking_operasi</h3>
          <br />
          <p>Fungsi : Mengambil kode booking operasi yang belum terlaksana</p>
          <p>Method : <b>POST</b></p>
          <p>Format : <b>Json</b></p>
          <p>Security   : <b>Token</b></p>
          <p>Header:     <b>Content-Type : application/json </b></p>
          <p>Header:     <b>x-customer : customer </b></p>
          <p>Header:     <b>x-token : token </b></p>
          <p>Parameter  1 : <b>nopeserta</b></p>
          <div id="getKodeBookingOperasiParam">
              <pre>
                <span class="cp">                               
     {
        "nopeserta" : "0000000000123"
     }
              </span>
              </pre>
             </div> 
               <p>Response</b></p>
           <div id="getListBookingOperasi">
            <pre>
              <span class="cp">
{
    "response": {
        "list": [
            {
                "kodebooking": "0080567",
                "tanggaloperasi": "2020-09-05",
                "jenistindakan": "operasi gigi",
                "kodepoli": "BDM",
                "namapoli": "GIGI BEDAH MULUT",
                "terlaksana": "0"
            }
        ]
    },
    "metadata": {
        "message": "Ok",
        "code": 200
    }
}
              </span>
           </pre> 
           <br>
          <button class="btn btn-danger" data-clipboard-target="#getKodeBookingOperasiParam" data-clipboard-action="copy">
              Copy to clipboard
          </button>    
      </div>
    </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo5">
      Get Jadwal Operasi
      </a>
    </div>
    <div id="collapseTwo5" class="collapse">
      <div class="card-body">
         <h3><i>{BASE URL}</i>/<i>Nama Service</i>/index.php/operasi/list_jadwal_operasi</h3>
          <br />
          <p>Fungsi : Mengambil jadwal operasi harian</p>
          <p>Method : <b>POST</b></p>
          <p>Format : <b>Json</b></p>
          <p>Security   : <b>Token</b></p>
          <p>Header:     <b>Content-Type : application/json </b></p>
          <p>Header:     <b>x-token : token </b></p>
          <p>Header:     <b>x-customer : customer </b></p>
          <p>Parameter  1 : <b>tanggalawal</b></p>
          <p>Parameter  2 : <b>tanggalakhir</b></p>
          <div id="getJadwalOperasiParam">
              <pre>
                <span class="cp">                               
     {
        "tanggalawal"  : "2020-09-05",
        "tanggalakhir" : "2020-09-05"
     }
              </span>
              </pre>
             </div> 
               <p>Response</b></p>
           <div id="getJawalOperasiAntrian">
            <pre>
              <span class="cp">
{
    "response": {
        "list": [
            {
                "kodebooking": "0080577",
                "tanggaloperasi": "2020-09-05",
                "jenistindakan": "operasi gigi",
                "kodepoli": "BDM",
                "namapoli": "gigi bedah mulut",
                "terlaksana": "1",
                "nopeserta": "000000000924782",
                "lastupdate": "1750823000"
            },
            {
                "kodebooking": "0080578",
                "tanggaloperasi": "2020-09-06",
                "jenistindakan": "operasi kulit",
                "kodepoli": "149",
                "namapoli": "tumor dan bedah kulit",
                "terlaksana": "1",
                "nopeserta": "000000000925722",
                "lastupdate": "1664423000"
            },
            {
                "kodebooking": "0080581",
                "tanggaloperasi": "2020-09-06",
                "jenistindakan": "bedah umum",
                "kodepoli": "BDA",
                "namapoli": "bedah anak",
                "terlaksana": "1",
                "nopeserta": "000000000935512",
                "lastupdate": "1664423000"
            }
        ]
    },
    "metadata": {
        "message": "Ok",
        "code": 200
    }
}
              </span>
           </pre> 
           <br>
          <button class="btn btn-danger" data-clipboard-target="#getJadwalOperasiParam" data-clipboard-action="copy">
              Copy to clipboard
          </button>    
      </div>
    </div>
    </div>
  </div>
              
            
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
     <!--  <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; <?php echo date("Y"); ?></span>
          </div>
        </div>
      </footer> -->
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->




  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo $url_assests;?>/assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo $url_assests;?>/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo $url_assests;?>/assets/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo $url_assests;?>/assets/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="<?php echo $url_assests;?>/assets/vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?php echo $url_assests;?>/assets/js/demo/chart-area-demo.js"></script>
  <script src="<?php echo $url_assests;?>/assets/js/demo/chart-pie-demo.js"></script>

</body>

</html>

