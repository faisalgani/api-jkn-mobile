<?php 
include('./config.php');
// echo $url_aplikasi;
// die();
$url=$url_aplikasi.'index.php/';
      $url_assests=$url_aplikasi; ?> 
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title> <?php echo $nama_aplikasi; ?> </title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo $url_assests;?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo $url_assests;?>assets/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo $url; ?>">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-code"></i>
        </div>
       
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo $url.'dashboard/update'; ?>">
          <i class="fa fa-list" aria-hidden="true"></i>
          <span>Update</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      

      <!-- Nav Item - Pages Collapse Menu -->
    <!-- <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
         <i class="fas fa-coins"></i>
          <span>Service</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="<?php echo $url . 'rest_server/generade/'; ?>">Generade Token</a>
          </div>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="<?php echo $url . 'rest_server/generade/'; ?>">No Antrian</a>
          </div>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="<?php echo $url . 'rest_server/generade/'; ?>">Rekap Antrian</a>
          </div>
        </div>
         <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="<?php echo $url . 'rest_server/generade/'; ?>">List Booking Operasi</a>
          </div>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="<?php echo $url . 'rest_server/generade/'; ?>">List Jadwal Operasi</a>
          </div>
        </div>
      </li> -->
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo $url.'dashboard'; ?>">
          <i class="fa fa-cog"></i>
          <span>Service</span></a>
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      

      <!-- Nav Item - Pages Collapse Menu -->
      

      <!-- Nav Item - Charts -->
      
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
          <h3><?php echo $nama_aplikasi; ?></h3>
        </nav>

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
          <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
          
          </form>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>         

        </nav>
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Generate Customer & Key</h1>
           
          </div>

          <!-- Content Row -->
          <div class="form-group">
             <label class="col-lg-2 control-label">Nama Customer</label>
                <div class="col-lg-5">
                  <input type="text" name="nama_customer" class="form-control" id="nama_customer" placeholder="Nama Customer" style="width:600px;">
                </div>
                    
          </div>
          <div class="form-group">
             <label class="col-lg-2 control-label">Deskripsi</label>
                <div class="col-lg-5">
                  <input type="text" name="deskripsi" class="form-control" id="deskripsi" placeholder="Deskripsi" style="width:600px;">
                </div>       
          </div>
           <div class="form-group">
            <div class="col-lg-5">
           <button type="submit" class="btn btn-primary pull-right" onclick="goGenerade()" >Generade</button>
           </div>
         </div>

         <div class="form-group">
             <label class="col-lg-2 control-label">Customer</label>
                <div class="col-lg-5">
                  <input type="text" name="customer_generade" class="form-control" id="customer_generade" placeholder="" style="width:600px;" readonly="true">
                </div>       
          </div>

           <div class="form-group">
             <label class="col-lg-2 control-label">Key</label>
                <div class="col-lg-5">
                  <input type="text" name="key_generade" class="form-control" id="key_generade" placeholder="" style="width:600px;" readonly="true">
                </div>       
          </div>

        </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
     <!--  <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; <?php echo date("Y"); ?></span>
          </div>
        </div>
      </footer> -->
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <div class="modal fade" id="modal_error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content panel-warning">
        <div class="modal-header panel-heading">
          <h5 align="center" class="modal-title"></h5>
        </div>
        <div class="modal-body">
          ...
        </div>

      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->



  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo $url_assests;?>/assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo $url_assests;?>/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo $url_assests;?>/assets/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo $url_assests;?>/assets/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="<?php echo $url_assests;?>/assets/vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="<?php echo $url_assests;?>/assets/js/demo/chart-area-demo.js"></script>
  <script src="<?php echo $url_assests;?>/assets/js/demo/chart-pie-demo.js"></script>

</body>

</html>

<script type="text/javascript">
  function goGenerade(){
    var nama_customer = $('#nama_customer').val();
    var deskripsi     = $('#deskripsi').val();
    if(nama_customer === "" || deskripsi === "" ){
      alert('Nama Customer atau Deskripsi Kosong!');
    }else{
        $.ajax({
            url : "<?php echo $url.'/rest_server/go_generade' ?>",
            type: "POST",
            data: {'nama_customer' :nama_customer,'deskripsi' :deskripsi},
            dataType: "JSON",
            success: function(data){
              console.log(data);
               if(data.status === true){
                 $('#customer_generade').val(data.customer_generade);
                 $('#key_generade').val(data.key_generad);
               }else{
                 $('#modal_error').modal('show'); 
                 $('.modal-title').text('Warning');
                 $('.modal-body').text(data.pesan);
               }
              
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error adding / update data');
            }
        });
    }
  
  }
</script>