<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MyModel extends CI_Model {

    var $content  = "application/json";
    var $auth_token = "x-token";
    var $auth_customer = "x-customer";

    public function check_auth_client(){
        $content = $this->input->get_request_header('Content-Type', TRUE);
        if($content == $this->content){
            return true;
        } else {
            $response['metadata']['message']='Unauthorized.';
            $response['metadata']['code']=401;
            return $response;
        }
    }
     public function check_auth_client_data(){
        $auth_token = $this->input->get_request_header('x-token', TRUE);
        $auth_customer = $this->input->get_request_header('x-customer', TRUE);
        if($auth_token != NULL && $auth_customer != NULL){
            $response['metadata']['code']=200;
            return $response;
        } else {
            $response['metadata']['message']='Unauthorized.';
            $response['metadata']['code']=401;
            return $response;
        }
    }

    public function login($username,$password){
        $q  = $this->db->select('password,username')->from('ws_users')->where(array('username'=>$username))->get()->row();
        if($q == ""){
              $response['metadata']['message']='User tidak ditemukan.';
              $response['metadata']['code']=204;
              return $response;
        } else {
            $iv = 'NciMedisJKN123';
            $customer_generade = $username;
            $key = $customer_generade;
            $key_generad = hash('sha1', (get_magic_quotes_gpc() ? stripslashes(base64_decode($key)) : base64_decode($key)));
            $pass_post = sha1($key_generad.$iv);
            $hashed_password = $q->password;
            $id              = $q->username;
            if ($hashed_password == $pass_post) {
                 $last_login = date('Y-m-d H:i:s');
                 $jam = date('i');
                 $now = date("is");
                 $hashed_password=$now.$hashed_password;
                 $token = crypt($now.substr( sha1($username), 0, 7),$hashed_password);
                 $expired_at = date("Y-m-d H:i:s", strtotime("+3 minutes"));
                 $this->db->trans_start();
                 $this->db->where(array('username'=>$id,'password'=>$pass_post))->update('ws_users',array('last_login' => $last_login));
                 $max_authentication = $this->db->query("SELECT MAX(id) id FROM ws_users_authentication");
                 if(count($max_authentication->result() ) == 0  ){
                      $id_urut=1;
                 }else{
                      $id_urut=$max_authentication->row()->id+1;
                 }
                 $cek =$this->db->query("SELECT * FROM ws_users_authentication where users_id = '".$username."' ");
                 if(count($cek->result()) >= 1 ){
                    $this->db->query("UPDATE ws_users_authentication set expired_at ='".$expired_at."' , token = '".$token."'
                          WHERE users_id ='".$username."' ");
                    if ($this->db->trans_status() === FALSE){
                          $this->db->trans_rollback();
                          $response['metadata']['message']='Internal server error.';
                          $response['metadata']['code']=500;
                          return $response;
                    } else {
                          $this->db->trans_commit();
                          $response['response']['token']=$token;
                          $response['metadata']['message']='Ok';
                          $response['metadata']['code']=200;
                          return $response;
                    }
                 }else{
                    $this->db->insert('ws_users_authentication',array('id'=>$id_urut,'users_id' => $id,'token' => $token,'expired_at' => $expired_at,'created_at'=> date("Y-m-d H:i:s")));
                       if ($this->db->trans_status() === FALSE){
                          $this->db->trans_rollback();
                          $response['metadata']['message']='Internal server error.';
                          $response['metadata']['code']=500;
                          return $response;
                       } else {
                          $this->db->trans_commit();
                          $response['response']['token']=$token;
                          $response['metadata']['message']='Ok';
                          $response['metadata']['code']=200;
                          return $response;
                       }
                 }
               
                
            } else {
                $response['metadata']['message']='User tidak ditemukan.';
                $response['metadata']['code']=204;
                return $response;
            }
        }
    }

    public function logout(){
        $users_id  = $this->input->get_request_header('User-ID', TRUE);
        $token     = $this->input->get_request_header('Authorization', TRUE);
        $this->db->where('users_id',$users_id)->where('token',$token)->delete('ws_users_authentication');
        $response['metadata']['message']='Successfully logout';
        $response['metadata']['code']=200;
        return $response;
    }

    public function save_user($data){
        $cek = $this->db->query("SELECT name FROM ws_users where name = '".$data['name']."' ")->result();
        if(count($cek) >= 1 ){
           return 'User Sudah Ada!';
        }else{
          $insert = $this->db->insert('ws_users',$data);
          if($insert){
            $this->db->trans_commit();
            return 1;
          }else{
            $this->db->trans_rollback();
            return 0;
          }
        }
        
    }

    public function auth(){
        $token     = $this->input->get_request_header('x-token', TRUE);
        $customer     = $this->input->get_request_header('x-customer', TRUE);
        $q  = $this->db->select('expired_at')->from('ws_users_authentication')->where(array('token' => $token, 'users_id' =>
          $customer))->get()->row();
        if($q == "" || $q == NULL){
            $response['metadata']['message']='Unauthorized';
            $response['metadata']['code']=401;
            return $response;
        } else {
            if($q->expired_at < date('Y-m-d H:i:s')){
                $del = $this->db->query("DELETE FROM ws_users_authentication where token ='$token' AND users_id ='$customer'");
                $response['metadata']['message']='Your session has been expired';
                $response['metadata']['code']=401;
                return $response;
            } else {
                $updated_at = date('Y-m-d H:i:s');
                $min =3;
                $expired_at = date("Y-m-d H:i:s", strtotime("+{$min} minutes"));
                $this->db->where(array('token'=>$token,'users_id'=>$customer))->update('ws_users_authentication',array('expired_at' => $expired_at,'updated_at' => $updated_at));
                $response['metadata']['message']='Authorized';
                $response['metadata']['code']=200;
                return $response;
            }
        }
    }

    public function get_data_antrian($params){
        $query=$this->db->query("SELECT a.no_antrian,a.ID_GROUP||'-'||f.KD_UNIT as no_boking,2 as jenis_antrian,
                '61260000' as estimasidilayani,f.nama_unit as namapoli,e.nama as namadokter   
                FROM
                ANTRIAN_LIST_ANTRIAN a
                INNER JOIN ANTRIAN_POLIKLINIK b ON a.ID_ANTRIAN= b.ID_ANTRIAN
                INNER JOIN KUNJUNGAN c ON c.KD_UNIT= b.KD_UNIT 
                AND c.KD_PASIEN= b.KD_PASIEN
                INNER JOIN PASIEN D on c.KD_PASIEN=d.KD_PASIEN
                inner join DOKTER e on e.KD_DOKTER=c.KD_DOKTER
                inner join UNIT f on f.KD_UNIT =c.KD_UNIT
                WHERE d.NO_ASURANSI = '".$params['nomorkartu']."'  AND c.NO_SJP ='".$params['nomorreferensi']."'")->result();
       return $query;
    }

    public function get_data_rekap_antrian($params){
      $query=$this->db->query("SELECT a.NAMA_UNIT AS namapoli,count( c.KD_PASIEN ) as totalantrean,
            count( b.KD_PASIEN ) as jumlahterlayani, '2780633000' as lastupdate
            -- CAST(Datediff(s, b.TGL_MASUK, GETUTCDATE()) AS BIGINT)*1000 as lastupdate
            FROM
            UNIT a
            INNER JOIN KUNJUNGAN b ON a.KD_UNIT= b.KD_UNIT 
            INNER JOIN ANTRIAN_POLIKLINIK c on c.KD_PASIEN=b.KD_PASIEN AND c.KD_UNIT=b.KD_UNIT
            WHERE
            a.KELAS_BPJS = '".$params['kodepoli']."'
            AND  b.TGL_MASUK = '".$params['tanggalperiksa']."' 
            GROUP BY a.NAMA_UNIT,b.TGL_MASUK")->result();
      return $query;
    }

    public function get_data_booking_operasi(){
      $query=$this->db->query("SELECT '0080567' as kodebooking, '2020-09-05' as tanggaloperasi, 'operasi gigi' as jenistindakan , 'BDM' as  kodepoli, 'GIGI BEDAH MULUT' as namapoli, '0' as terlaksana ")->result();
      return $query;
    }

    public function get_data_jadwal_operasi(){

       $query=$this->db->query("SELECT '0080577' as kodebooking, '2020-09-05' as tanggaloperasi, 'operasi gigi' as jenistindakan , 'BDM' as kodepoli, 'gigi bedah mulut' as namapoli, '1' as terlaksana, '000000000924782' as nopeserta, '1750823000' as lastupdate 
          UNION ALL 

          SELECT '0080578' as kodebooking, '2020-09-06' as tanggaloperasi, 'operasi kulit' as jenistindakan , '149' as kodepoli, 'tumor dan bedah kulit' as namapoli, '1' as terlaksana, '000000000925722' as nopeserta, '1664423000' as lastupdate 
          UNION ALL 

          SELECT '0080581' as kodebooking, '2020-09-06' as tanggaloperasi, 'bedah umum' as jenistindakan , 'BDA' as kodepoli, 'bedah anak' as namapoli, '1' as terlaksana, '000000000935512' as nopeserta, '1664423000' as lastupdate")->result();  
       return $query;
    }
    

}
