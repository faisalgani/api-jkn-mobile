<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class operasi extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        /*
        	$check_auth_client = $this->MyModel->check_auth_client();
		if($check_auth_client != true){
			die($this->output->get_output());
		}
		*/
    }

	public function kode_booking_operasi(){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$respStatus = 400;
			$resp['metadata']['message']='Bad request.';
		    $resp['metadata']['code']=400;
		    json_output($respStatus,$resp);
		} else {
			$check_auth_client = $this->MyModel->check_auth_client_data();
			if($check_auth_client['metadata']['code'] == 200){
		        $response = $this->MyModel->auth();
		        $respStatus = $response['metadata']['code'];
		        if($response['metadata']['code'] == 200){
					$params = json_decode(file_get_contents('php://input'), TRUE);
					if ($this->validasiGetBookingOperasiBody($params) != true) {
						$respStatus = 400;
	                    $resp['metadata']['message']='Format json salah!';
			               $resp['metadata']['code']=400;	
			               json_output($respStatus,$resp);
					} else {
						if($this->validasiGetBookingOperasi($params) != true){
							$respStatus = 400;
		                    $resp['metadata']['message']='Format json salah!';
				            $resp['metadata']['code']=400;	
				            json_output($respStatus,$resp);
						}else{
							$respStatus=200;
				        	$data = $this->MyModel->get_data_booking_operasi($params);
				        	if(count($data) > 0 ){
				        		$resp['response']['list']=$data;
		                    	$resp['metadata']['message']='Ok';
				                $resp['metadata']['code']=200;
				                json_output($respStatus,$resp);
				        	}else{
				        		$resp['metadata']['message']='Data tidak ditemukan.';
					            $resp['metadata']['code']=412;	
					            json_output(412,$resp);
				        	}	
				        	
						}
					}
					json_output($respStatus,$resp);
		        }
			}else{
				json_output(401,$check_auth_client);
			}
		}
		
	}

	public function list_jadwal_operasi(){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$respStatus = 400;
			$resp['metadata']['message']='Bad request.';
		    $resp['metadata']['code']=400;
		    json_output($respStatus,$resp);
		} else {
			$check_auth_client = $this->MyModel->check_auth_client_data();
			if($check_auth_client['metadata']['code'] == 200){
		       $response = $this->MyModel->auth();
		       $respStatus = $response['metadata']['code'];
		       if($response['metadata']['code'] == 200){
					$params = json_decode(file_get_contents('php://input'), TRUE);
					if ($this->validasiGetListJadwalOperasiBody($params) != true) {
						$respStatus = 400;
						$resp['metadata']['message']='Format json salah!';
				        $resp['metadata']['code']=400;	
				        json_output($respStatus,$resp);
					}else {
						if ($this->validasiGetListJadwalOperasi($params) != true) {	
							$respStatus = 400;
							$resp['metadata']['message']='Format json salah!';
					        $resp['metadata']['code']=400;	
					        json_output($respStatus,$resp);
						}else{
					        $data = $this->MyModel->get_data_jadwal_operasi($params);
					        if(count($data) > 0 ){
					        	$respStatus=200;
					        	$resp['response']['list']=$data;
		                   		$resp['metadata']['message']='Ok';
			                	$resp['metadata']['code']=200;
			                 	json_output($respStatus,$resp);
					        }else{
					        	$resp['metadata']['message']='Data tidak ditemukan.';
					            $resp['metadata']['code']=412;	
					            json_output(412,$resp);
					        }
					        
						}
						
					}
				json_output($respStatus,$resp);
		       }
			}else{
				json_output(401,$check_auth_client);
			}
		}
	}

	private function validasiGetBookingOperasiBody($params){
		if (isset($params['nopeserta']) == "" ) {
			return  false;
		}else{
			return true;
		}	
	}

	private function validasiGetBookingOperasi($params){
		if ($params['nopeserta'] == "" ) {
			return  false;
		}else{
			return true;
		}	
	}
	private function validasiGetListJadwalOperasi($params){
		if ($params['tanggalawal'] == "" && $params['tanggalakhir'] == "") {
			return  false;
		}else{
			if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$params['tanggalawal']) &&
				preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$params['tanggalakhir']) ) {
			    return true;
			} else {
			    return false;
			}
		}
	}

	private function validasiGetListJadwalOperasiBody($params){
		if (isset($params['tanggalawal']) == "" && isset($params['tanggalakhir']) == "") {
			return  false;
		}else{
			return true;	
		}
	}

}
