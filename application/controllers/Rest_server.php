<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class rest_server extends CI_Controller {

	public function __construct() {
        parent::__construct();
    }

    public function index(){
    	$this->load->database();
      $this->load->helper('url');
      $this->load->view('index');
    }

    public function generate(){
    	$this->load->database();
      $this->load->helper('url');
      $this->load->view('generade_token');
    }

    public function go_generade(){
       $this->load->database();
       $this->db->trans_begin();
       $nama_customer = $this->input->post('nama_customer');
       $deskripsi = $this->input->post('deskripsi');
       $customer_generade = rand(10000,99999);
       $iv = 'NciMedisJKN123';
       $key = $customer_generade;
       $key_generad = hash('sha1', (get_magic_quotes_gpc() ? stripslashes(base64_decode($key)) : base64_decode($key)));
       $data = array(
              'username' => $customer_generade,
              'password'=>sha1($key_generad.$iv),
              'name'=>$nama_customer,
              'created_at'=> date("Y-m-d H:i:s"),
              'deskripsi' => $deskripsi );
       $insert= $this->MyModel->save_user($data);
       if($insert == 1){
          echo json_encode(array("status" => TRUE,"customer_generade" => $customer_generade,"key_generad" => $key_generad));   
       }else{
           echo json_encode(array("status" => FALSE,"pesan" => $insert));   
       }

          
    }
}
