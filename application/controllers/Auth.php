<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function login(){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$response['metadata']['message']='Bad request.';
            $response['metadata']['code']=400;
            json_output(301,$response);
		} else {
			$params = json_decode(file_get_contents('php://input'), TRUE);
			$check_auth_client = $this->MyModel->check_auth_client();
			if(isset($params['customer']) == false  || isset($params['key']) == false ){
				$response['metadata']['message']='Parameter Tidak Lengkap';
                $response['metadata']['code']=301;
                json_output(301,$response);
			}else{
				if($check_auth_client == true){
			        $username = $params['customer'];
			        $password = $params['key'];
			        $response = $this->MyModel->login($username,$password);
					json_output($response['metadata']['code'],$response);
				}
			}
			
		}
	}

	public function logout(){	
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->MyModel->check_auth_client();
			if($check_auth_client == true){
		        	$response = $this->MyModel->logout();
				json_output($response['status'],$response);
			}
		}
	}
	
}
