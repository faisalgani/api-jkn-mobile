<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class antrian extends CI_Controller {

	public function __construct(){
        parent::__construct();
    }

	public function index(){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$respStatus = 400;
			$resp['metadata']['message']='Bad request.';
		    $resp['metadata']['code']=400;
		    json_output($respStatus,$resp);
		} else {
			$check_auth_client = $this->MyModel->check_auth_client_data();
			if($check_auth_client['metadata']['code'] == 200){
		        	$response = $this->MyModel->auth();
		        	$respStatus = $response['metadata']['code'];
		        	if($response['metadata']['code'] == 200){
						$params = json_decode(file_get_contents('php://input'), TRUE);
						if ($this->validasiGetNoAntrianBody($params) == false ) {
							$respStatus = 400;
	                    	$resp['metadata']['message']='Format json salah!';
			                $resp['metadata']['code']=$respStatus;	
			                json_output($respStatus,$resp);
						} else {
							if($this->validasiGetNoAntrian($params) == true){
				        		$data = $this->MyModel->get_data_antrian($params);
				        		if(count($data) > 0 ){
				        			$respStatus=200;
				        			$resp['response']=$data;
		                    		$resp['metadata']['message']='Ok';
				                    $resp['metadata']['code']=$respStatus;	
				                    json_output($respStatus,$resp);
				        		}else{
				        			$respStatus=412;
				        			$resp['metadata']['message']='Data tidak ditemukan.';
					                $resp['metadata']['code']=412;	
					                json_output($respStatus,$resp);
				        		}
							}else{
								$resp['metadata']['message']='Format json salah!';
				                $resp['metadata']['code']=$respStatus;	
				                json_output($respStatus,$resp);
							}
						}
		        	}else{
		        		$respStatus=401
		        		$resp['metadata']['message']='Your session has been expired';
		                $resp['metadata']['code']=$respStatus;
		                json_output($respStatus,$resp);
		        	}
		        	json_output($respStatus,$resp);
			}else{
				json_output(401,$check_auth_client);
			}
		}
	}

	public function rekap_antrian(){
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			$respStatus = 400;
			$resp['metadata']['message']='Bad request.';
		    $resp['metadata']['code']=400;
		    json_output($respStatus,$resp);
		} else {
			$check_auth_client = $this->MyModel->check_auth_client_data();
			if($check_auth_client['metadata']['code'] == 200){
		        	$response = $this->MyModel->auth();
		        	$respStatus = $response['metadata']['code'];
		        	if($response['metadata']['code'] == 200){
						$params = json_decode(file_get_contents('php://input'), TRUE);
						if ($this->validasiGetRekapAntrianBody($params) == false ) {
							$respStatus = 400;
	                    	$resp['metadata']['message']='Format json salah!';
			                $resp['metadata']['code']=400;	
			                json_output($respStatus,$resp);
						} else {
							if($this->validasiGetRekapAntrian($params) == true){
				        		$data = $this->MyModel->get_data_rekap_antrian($params);
				        		if(count($data) > 0 ){
				        			$respStatus=200;
				        			$resp['response']=$data;
	                    			$resp['metadata']['message']='Ok';
			                   		$resp['metadata']['code']=200;	
			                   		json_output($respStatus,$resp);
				        		}else{
				        			$resp['metadata']['message']='Data tidak ditemukan.';
					                $resp['metadata']['code']=412;	
					                json_output(412,$resp);
				        		}
							}else{
								$resp['metadata']['message']='Format json salah!';
				                $resp['metadata']['code']=400;	
				                json_output(412,$resp);
							}
						}
		        	}else{
		        		$resp['metadata']['message']='Your session has been expired';
		                $resp['metadata']['code']=401;
		                json_output(401,$resp);
		        	}
		        	json_output($respStatus,$resp);
			}else{
				json_output(401,$check_auth_client);
			}
		}
	}

	/*public function create()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'POST'){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->MyModel->check_auth_client();
			if($check_auth_client == true){
		        	$response = $this->MyModel->auth();
		        	$respStatus = $response['status'];
		        	if($response['status'] == 200){
					$params = json_decode(file_get_contents('php://input'), TRUE);
					if ($params['title'] == "" || $params['author'] == "") {
						$respStatus = 400;
						$resp = array('status' => 400,'message' =>  'Title & Author can\'t empty');
					} else {
		        			$resp = $this->MyModel->book_create_data($params);
					}
					json_output($respStatus,$resp);
		        	}
			}
		}
	}
*/
	/*public function update($id)
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'PUT' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->MyModel->check_auth_client();
			if($check_auth_client == true){
		        	$response = $this->MyModel->auth();
		        	$respStatus = $response['status'];
				if($response['status'] == 200){
					$params = json_decode(file_get_contents('php://input'), TRUE);
					$params['updated_at'] = date('Y-m-d H:i:s');
					if ($params['title'] == "" || $params['author'] == "") {
						$respStatus = 400;
						$resp = array('status' => 400,'message' =>  'Title & Author can\'t empty');
					} else {
		        			$resp = $this->MyModel->book_update_data($id,$params);
					}
					json_output($respStatus,$resp);
		       		}
			}
		}
	}
*/
	/*public function delete($id)
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if($method != 'DELETE' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE){
			json_output(400,array('status' => 400,'message' => 'Bad request.'));
		} else {
			$check_auth_client = $this->MyModel->check_auth_client();
			if($check_auth_client == true){
		        	$response = $this->MyModel->auth();
		        	if($response['status'] == 200){
		        		$resp = $this->MyModel->book_delete_data($id);
					json_output($response['status'],$resp);
		        	}
			}
		}
	}*/

	private function validasiGetNoAntrian($params){
		if ($params['nomorkartu'] == "" || 
			$params['nik'] == "" ||  
			$params['notelp'] == "" ||  
			$params['tanggalperiksa'] == ""||  
			$params['kodepoli'] == "" ||  
			$params['nomorreferensi'] == ""|| 
			$params['jenisreferensi'] == "" ||  
			$params['jenisrequest'] == "" ||  
			$params['polieksekutif'] == "" ) {
			return  false;
		}else{
			return true;
		}
		
	}

	private function validasiGetNoAntrianBody($params){
		if (isset($params['nomorkartu']) == ""   ||  
			isset($params['nik']) == ""  || 
			isset($params['notelp']) == "" || 
			isset($params['tanggalperiksa']) == ""  || 
			isset($params['kodepoli']) == ""   || 
			isset($params['nomorreferensi']) == ""  ||
			isset($params['jenisreferensi']) == ""  ||
			isset($params['jenisrequest']) == ""  || 
			isset($params['polieksekutif']) == ""  ) {
			return  false;
		}else{
			return true;
		}
	}

	private function validasiGetRekapAntrian($params){
		if ($params['tanggalperiksa'] == "" || $params['kodepoli'] == "" || $params['polieksekutif'] == "" ) {
			return  false;
		}else{
			return true;
		}
		
	}

	private function validasiGetRekapAntrianBody($params){
		if (isset($params['tanggalperiksa']) == "" || 
			isset($params['kodepoli']) == "" || 
			isset($params['polieksekutif']) == "" ) {
			return  false;
		}else{
			return true;
		}
	}
}
