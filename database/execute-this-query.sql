/*
 Navicat Premium Data Transfer

 Source Server         : postgre sql local
 Source Server Type    : PostgreSQL
 Source Server Version : 90300
 Source Host           : localhost:5432
 Source Catalog        : db_jailolo
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 90300
 File Encoding         : 65001

 Date: 02/07/2021 14:38:16
*/


-- ----------------------------
-- Table structure for ws_users
-- ----------------------------
DROP TABLE IF EXISTS "public"."ws_users";
CREATE TABLE "public"."ws_users" (
  "username" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "password" varchar(100) COLLATE "pg_catalog"."default",
  "name" varchar(30) COLLATE "pg_catalog"."default",
  "last_login" timestamp(6),
  "created_at" timestamp(6),
  "updated_at" timestamp(6),
  "deskripsi" text COLLATE "pg_catalog"."default"
);


ALTER TABLE "public"."ws_users" ADD CONSTRAINT "ws_users_pkey" PRIMARY KEY ("username");

-- ----------------------------
-- Records of ws_users
-- ----------------------------
INSERT INTO "public"."ws_users" VALUES ('88969', '48cb97056058085a442fbf61d028c67d86c6e2c1', 'Bpjs Kabanjahe', '2021-07-01 14:58:13', '2021-07-01 14:17:45', NULL, 'Bpjs Kabanjahe');
INSERT INTO "public"."ws_users" VALUES ('79106', 'fc3f8b69a9e4777bcd0c32d040f06995f1a02037', 'Bpjs Jailolo', '2021-07-01 14:58:30', '2021-07-01 14:57:40', NULL, 'Bpjs Jailolo');
INSERT INTO "public"."ws_users" VALUES ('60136', '3c62377133d3e695dc9e50a59da1e55918721df6', 'Bpjs Medan', '2021-07-02 14:24:03', '2021-07-01 15:16:16', NULL, 'Bpjs Medan');

-- ----------------------------
-- Primary Key structure for table ws_users
-- ----------------------------

DROP TABLE IF EXISTS "public"."ws_users_authentication";
CREATE TABLE "public"."ws_users_authentication" (
  "id" int4 NOT NULL,
  "users_id" int4,
  "token" varchar(255) COLLATE "pg_catalog"."default",
  "expired_at" timestamp(6),
  "created_at" timestamp(6),
  "updated_at" timestamp(6)
);

ALTER TABLE "public"."ws_users_authentication" 
ADD PRIMARY KEY ("id");

-- ----------------------------
-- Records of ws_users_authentication
-- ----------------------------
INSERT INTO "public"."ws_users_authentication" VALUES (3, 60136, '24oRIVrPXJpjs', '2021-07-02 14:27:03', '2021-07-02 11:15:01', '2021-07-02 14:11:16');
INSERT INTO "public"."ws_users_authentication" VALUES (1, 88969, '588Gt2N7SJg3A', '2021-07-01 15:01:13', NULL, '2021-07-01 14:57:12');
INSERT INTO "public"."ws_users_authentication" VALUES (2, 79106, '58qoM99F/1TJQ', '2021-07-01 15:01:30', NULL, NULL);



ALTER TABLE "public"."antrian_list_antrian" 
ADD COLUMN "id_antrian" int2 NOT NULL,
ADD CONSTRAINT "antrian_list_antrian_pkey" PRIMARY KEY ("id_antrian", "id_antrian");


ALTER TABLE "public"."antrian_poliklinik" 
ADD COLUMN "id_antrian" int2; 
